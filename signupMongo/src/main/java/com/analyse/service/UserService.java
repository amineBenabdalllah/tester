package com.analyse.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.analyse.model.MyUser;
import com.analyse.repository.UserRespository;



@Service
public class UserService {

    @Autowired
    private UserRespository userRepository;

    public MyUser findUserByUserName(String username){
        return userRepository.findByUsername(username);
    }
    public void createUser(MyUser newUser) {
        userRepository.save(newUser);
    }
}
