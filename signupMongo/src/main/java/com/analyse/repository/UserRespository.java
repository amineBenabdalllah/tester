package com.analyse.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.analyse.model.MyUser;

public interface UserRespository extends MongoRepository<MyUser, String>{
	public MyUser findByUsername(String username);

}
