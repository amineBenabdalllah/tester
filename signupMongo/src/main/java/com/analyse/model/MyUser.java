package com.analyse.model;


import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;


@Document
public class MyUser {

	@Id
	private String username;
	
	private String name;
	
	private String password;
	
	private String email;
	
	private String role;
	
	private Date date;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public MyUser(String username, String name, String password, String email, String role, Date date) {
		super();
		this.username = username;
		this.name = name;
		this.password = password;
		this.email = email;
		this.role = role;
		this.date = date;
	}

	public MyUser() {
		super();
	}
	
	
	
}
